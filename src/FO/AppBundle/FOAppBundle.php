<?php

namespace FO\AppBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class FOAppBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
