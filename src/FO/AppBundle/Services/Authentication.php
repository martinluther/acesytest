<?php

namespace FO\AppBundle\Services;

use FOS\UserBundle\Doctrine\UserManager;
use JMS\Serializer\SerializerBuilder;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;

/**
 * Created by PhpStorm.
 * User: wilson
 * Date: 26/10/17
 * Time: 17:48
 */

class Authentication
{
    private $user_manager;

    private $factory;


    public function __construct(EncoderFactory $factory, UserManager $user_manager)
    {
        $this->factory = $factory;
        $this->user_manager = $user_manager;
    }

    public function authenticate($username, $password)
    {
        $user = $this->user_manager->findUserByUsername($username);

        if ($user==null){
            return new JsonResponse('aucun user');
        }else{
            $encoder = $this->factory->getEncoder($user);
            $rep = $encoder->isPasswordValid($user->getPassword(),$password,$user->getSalt());
            if ($rep){
                $serializer = SerializerBuilder::create()->build();
                $jsonContent = $serializer->serialize($user, 'json');
                return new  Response($jsonContent, Response::HTTP_OK, array('Content-Type' => 'application/json'));
            }else{
                return new JsonResponse('false');
            }
        }
    }

}